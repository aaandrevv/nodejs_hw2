const mongoose = require('mongoose');

const note = mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
}, {timestamps: true});

const Note = mongoose.model('Note', note);

module.exports = {Note};
