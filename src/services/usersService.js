const bcrypt = require('bcryptjs');
const {User} = require('../models/Users');

const getInfo = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {_id, username, createdAt} = await User.findOne({
      _id: userId,
    });
    res.status(200).send({
      user: {
        _id,
        username,
        createdDate: createdAt,
      },
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const deleteProfile = async (req, res, next) => {
  try {
    const {userId} = req.user;
    await User.deleteOne({
      _id: userId,
    });
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const changeProfilePassword = async (req, res, next) => {
  try {
    const {oldPassword, newPassword} = req.body;
    const {userId} = req.user;

    const user = await User.findOne({
      _id: userId,
    });
    if (!(await bcrypt.compare(String(oldPassword), String(user.password)))) {
      throw new Error('Password is incorrect');
    }
    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

module.exports = {getInfo, deleteProfile, changeProfilePassword};
