const {User} = require('../models/Users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const createProfile = async (req, res, next) => {
  try {
    const {username, password} = req.body;

    const user = new User({
      username,
      password: await bcrypt.hash(password, 10),
    });

    await user.save();
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const login = async (req, res, next) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});
  if (user && await bcrypt.compare(String(password), String(user.password))) {
    const payload = {username: user.username, userId: user._id};
    const token = jwt.sign(payload, process.env.SECRET_KEY);
    res.status(200).send({
      message: 'Success',
      jwt_token: token,
    });
  } else {
    res.status(400).send({
      message: 'Bad auth',
    });
  }
};

module.exports = {createProfile, login};
