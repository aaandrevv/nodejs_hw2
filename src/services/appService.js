const axios = require('axios');
const jwt = require('jsonwebtoken');

const _apiBase = `http://localhost:${process.env.PORT}/api`;

const renderHomePage = async (req, res, next) => {
  try {
    const authorization = req.cookies.token;
    if (!authorization) {
      return res.render('index', {
        user: null,
        notes: null,
      });
    }
    const user = jwt.verify(authorization.split(' ')[1],
        process.env.SECRET_KEY);
    const {data: {notes}} = await axios({
      method: 'get',
      url: `${_apiBase}/notes`,
      headers: {
        authorization: authorization,
      },
    });
    res.render('index', {
      user,
      notes,
    });
  } catch (err) {
    return renderErrorPage(400, err.message)(req, res);
  }
};

const logIn = async (req, res, next) => {
  const {username, password} = req.body;
  try {
    const {data: {jwt_token}} = await axios({
      method: 'post',
      url: `${_apiBase}/auth/login`,
      data: {
        username,
        password,
      },
    });
    res.cookie('token', `Bearer ${jwt_token}`);
    res.redirect('back');
  } catch (err) {
    return renderErrorPage(400, `${err.message}. Check if your username and password are correct`)(req, res);
  }
};

const register = async (req, res, next) => {
  try {
    const {username, password} = req.body;
    await axios({
      method: 'post',
      url: `${_apiBase}/auth/register`,
      data: {
        username,
        password,
      },
    });
    return logIn(req, res, next);
  } catch (err) {
    return renderErrorPage(400, `${err.message}. Your username must be unique`)(req, res);
  }
};

const logOut = async (req, res, next) => {
  try {
    res.clearCookie('token');
    res.redirect('back');
  } catch (err) {
    return renderErrorPage(400, err.message)(req, res);
  }
};

const renderErrorPage = (statusCode, statusTitle) => {
  return async (req, res) => res.status(statusCode).render('error', {
    title: statusTitle,
    user: req.user,
  });
};

const changePassword = async (req, res, next) => {
  const {oldPassword, newPassword} = req.body;
  try {
    await axios({
      method: 'patch',
      url: `${_apiBase}/users/me`,
      data: {
        oldPassword,
        newPassword,
      },
      headers: {
        authorization: req.cookies.token,
      },
    });
    res.redirect('back');
  } catch (err) {
    return renderErrorPage(400, err.message)(req, res);
  }
};

const postNote = async (req, res, next) => {
  const {text} = req.body;
  try {
    await axios({
      method: 'post',
      url: `${_apiBase}/notes`,
      headers: {
        authorization: req.cookies.token,
      },
      data: {
        text,
      },
    });
    res.status(200).redirect('back');
  } catch (err) {
    return renderErrorPage(400, err.message)(req, res);
  }
};

const deleteNote = async (req, res, next) => {
  try {
    const {_id} = req.params;
    await axios({
      method: 'delete',
      url: `${_apiBase}/notes/${_id}`,
      headers: {
        authorization: req.cookies.token,
      },
    });
    res.redirect('/');
  } catch (err) {
    return renderErrorPage(400, err.message)(req, res);
  }
};

const checkNote = async (req, res, next) => {
  try {
    const {_id} = req.params;
    await axios({
      method: 'patch',
      url: `${_apiBase}/notes/${_id}`,
      headers: {
        authorization: req.cookies.token,
      },
    });
    res.redirect('/');
  } catch (err) {
    return renderErrorPage(400, err.message)(req, res);
  }
};

const editNote = async (req, res, next) => {
  try {
    const {_id} = req.params;
    const {text} = req.body;

    await axios({
      method: 'put',
      url: `${_apiBase}/notes/${_id}`,
      headers: {
        authorization: req.cookies.token,
      },
      data: {
        text,
      },
    });
    res.redirect('/');
  } catch (err) {
    return renderErrorPage(400, err.message)(req, res);
  }
};

const deleteAccount = async (req, res, next) => {
  try {
    await axios({
      method: 'delete',
      url: `${_apiBase}/users/me`,
      headers: {
        authorization: req.cookies.token,
      },
    });
    res.clearCookie('token');
    res.redirect('/');
  } catch (err) {
    return renderErrorPage(400, err.message)(req, res);
  }
};

module.exports = {renderHomePage,
  logIn,
  register,
  logOut,
  changePassword,
  postNote,
  deleteNote,
  checkNote,
  editNote,
  deleteAccount};

