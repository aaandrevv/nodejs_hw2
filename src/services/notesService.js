const {Note} = require('../models/Notes');

const getNotes = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {offset, limit} = req.query;
    let notes = (await Note.find({userId})).slice(offset);
    if (limit) {
      notes = notes.slice(0, limit);
    }
    res.status(200).send({
      offset,
      limit,
      count: notes.length,
      notes,
    });
  } catch (error) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const addNote = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {text} = req.body;
    const note = new Note({
      userId,
      text,
    });
    await note.save();
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const getNote = async (req, res, next) => {
  try {
    const {_id} = req.params;
    const {userId, completed, text, createdAt} = await Note.findOne({_id});
    res.status(200).send({
      note: {
        _id,
        userId,
        completed,
        text,
        createdAt,
      },
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const editNote = async (req, res, next) => {
  try {
    const {_id} = req.params;
    const {text} = req.body;
    await Note.findOneAndUpdate({_id}, {$set: {text}});
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const checkNote = async (req, res, next) => {
  try {
    const {_id} = req.params;
    await Note.findOneAndUpdate(
        {_id},
        [{$set: {completed: {$not: '$completed'}}}]);
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

const deleteNote = async (req, res, next) => {
  try {
    const {_id} = req.params;
    await Note.deleteOne({_id});
    res.status(200).send({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).send({
      message: err.message,
    });
  }
};

module.exports = {getNotes, addNote, getNote, editNote, checkNote, deleteNote};
