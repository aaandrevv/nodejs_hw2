const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const {authorization} = req.headers;

  if (!authorization) {
    return res.status(401).send({
      message: 'Please provide authorization header',
    });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(401).send({
      message: 'Please provide token',
    });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
    req.user = {
      userId: tokenPayload.userId,
      username: tokenPayload.username,
    };
    next();
  } catch (err) {
    res.status(401).send({
      message: err.message,
    });
  }
};

module.exports = {authMiddleware};
