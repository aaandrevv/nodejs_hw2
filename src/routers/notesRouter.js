const express = require('express');

const router = express.Router();

const {getNotes,
  addNote,
  getNote,
  editNote,
  checkNote,
  deleteNote} = require('../services/notesService');

const {authMiddleware} = require('../middlewares/authMiddleware');

router.get('/', authMiddleware, getNotes);

router.post('/', authMiddleware, addNote);

router.get('/:_id', authMiddleware, getNote);

router.put('/:_id', authMiddleware, editNote);

router.patch('/:_id', authMiddleware, checkNote);

router.delete('/:_id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
