const express = require('express');

const router = express.Router();

const {getInfo,
  deleteProfile,
  changeProfilePassword} = require('../services/usersService');

const {authMiddleware} = require('../middlewares/authMiddleware');

router.get('/me', authMiddleware, getInfo);

router.delete('/me', authMiddleware, deleteProfile);

router.patch('/me', authMiddleware, changeProfilePassword);

module.exports = {
  usersRouter: router,
};
