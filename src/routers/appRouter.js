const express = require('express');

const {renderHomePage,
  logIn,
  register,
  logOut,
  changePassword,
  postNote,
  deleteNote,
  checkNote,
  editNote,
  deleteAccount} = require('../services/appService');

const router = express.Router();

router.get('/', renderHomePage);

router.post('/login', logIn);

router.post('/register', register);

router.get('/logout', logOut);

router.post('/changePassword', changePassword);

router.post('/postNote', postNote);

router.post('/deleteNote/:_id', deleteNote);

router.post('/checkNote/:_id', checkNote);

router.post('/editNote/:_id', editNote);

router.get('/deleteAccount', deleteAccount);

module.exports = {
  appRouter: router,
};
