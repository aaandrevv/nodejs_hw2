const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
require('dotenv').config();

const uri = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.h00tq4v.mongodb.net/?retryWrites=true&w=majority`;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('tiny'));
app.use(cookieParser());
app.use(express.static('public'));

const {authRouter} = require('./routers/authRouter');
const {usersRouter} = require('./routers/usersRouter');
const {notesRouter} = require('./routers/notesRouter');
const {appRouter} = require('./routers/appRouter');

app.set('view engine', 'ejs');
app.set('views', './src/views');

app.use('/', appRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    await mongoose.connect(uri);
    const port = process.env.PORT || 8080;
    app.listen(port, () => {
      console.log(`Server has been started on port ${port}`);
    });
  } catch (err) {
    console.log(`Error on server startup: ${err.message}`);
  }
};

start();
